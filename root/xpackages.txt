# This list was formed by running the following commands in the base image:
#   for package in $(rpm -qa); do if dnf erase $package; then echo "$package" >> remove; fi; done
#   cat remove
# Including those packages in this file.  Finally, repeatedly running the container build
# untill no dependency errors were raised.

criu
criu-libs
crypto-policies-scripts
dejavu-sans-fonts
findutils
fonts-filesystem
gdb-gdbserver
langpacks-core-en
langpacks-core-font-en
langpacks-en
libnet
protobuf-c
rootfiles
vim-minimal
yum
